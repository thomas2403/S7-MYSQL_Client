# S7-MYSQL_Client
___
**Client for accesing MySQL databases from Siemens S7-1200/1500 CPUs written in SCL, developed with TIA Portal V16**  

**Introduction**

> This is a client accesing MySQL databases from Siemens S7-1200/1500 CPUs. I have tested it with a S7-1511 (FW 2.8) and a S7-1214 CPU (FW 4.4).<br/>
> I have no idea, if it will work with lower Firmware versions or TIA below V16.<br/>
> I wrote this for myself and I found it it a good idea to share the code. Other than Microsoft SQL Server, there are not so many examples for MySQL.<br/>
> The code is barely tested and there is a good chance, that it will have some errors.<br/>
> I'm not responsible for any problems or damages which are caused by using this software. (See LICENSE file)<br/>
> Don't try to force me to implement enhancements. My time is very limited. If I find it usefull for me, I will eventually implement it some day.<br/>
> If you really need it, feel free to fork this repo and do it on your own.<br/>
> The same goes for pull requests. It depends, if I find it usefull, if I like the way the changes were made and if I have some some time to review and test it.


**License:**

> BSD 2-Clause
>
> _For details, see LICENSE file_

**Target:** 

> S7-1500 / FW 2.8 or S7-1220 / FW 4.4

**Limitations:**

  * only CLIENT_PROTOCOL_41
  * only Native Authentication (Native41)
  * SSL is not supported
  * only CALL, UPDATE, INSERT, DELETE, SELECT are supoorted
  * no Prepared Statements
  * no Multi-Resultset or Multi-Statements
  * max. length of column is 254

**Inputs:**

  * **Init : Bool** - _Resets some variables at start,usually connected to FIRSTSCAN_
  * **Connect : Bool** - _Connects to the database when set, disconnects when not set_
  * **Host : IP_V4** - _IP address of the host_
  * **Port : UInt** - _Port host of the database, usually 3306_
  * **Conn_ID : CONN_OUC** - _Connection ID_
  * **User : String** - _database user name_
  * **Password : String** - _user's password_
  * **Database : String** - _name of the databse to connect to (not the host name)_
  * **Command : String** - _command string to execute_
  * **Execute : Bool** - _execute the command string with rising edge_

**Outputs:**

  * **Connected  : Bool** - _TCP conneceion to the host established_
  * **Conn_Error_Stat : Word** - _error number for connection to the host_
  * **Logged_In : Bool** - _connected to the databse and logged in_
  * **Ready : Bool** - _ready for execution of SQL commands_
  * **Cmd_OK : Bool** - _execution of SQL command was OK_
  * **Cmd_Errror : Bool** - _execution of SQL command got errors_
  * **SQL_Error_Stat : Word** - _error number for command execution_
  * **SQL_Error_Msg : String** - _Error as readable text, only valid when **Cmd_Error** is set_
  * **Affected_Rows : DInt** - _affected rows for INSERT, UPADTE, DELETE or number of rows found for SELECT_
  * **Last_Row : DInt** - _last row after INSERT_
  * **ColCount : UInt** - _number of columns for SELECT_
  * **ColNames_Avail : Bool** - _names of columns in **Columns** (I/O) available when SELECT is executed_
  * **New_Query_Record : Bool** - _new row string in **Rows** (I/O) is available when SELECT is executed_


**In/Outs:**

  * **Columns : String** - _Column string, all column names separated with TAB ($T) when executing SELECT_
  * **Rows : String** - _Row string, all column values separated with TAB ($T) when executing SELECT_
  * **Conn_DB : Variant** - _Connection DB for TCON, TDISCON, TRCV, TSEND_

**Usage:**

1. make sure, that all inputs are connected and **Init** was set once after start or after changing the instance
2. set **Connect** to 1, **Connected** will get active, otherwise the error code of TCON or TRCV will be shown on **Conn_Error_Stat**
3. the login process starts automatically then, if successfull **Logged_In** gets activated, otherwise the connection will be terminated
4. now **Ready** should be active to show, that an SQL command can be executed
5. with the rising edge on **Execute** the SQL command on **Command** is executed, **Cmd_OK** and **Cmd_Errror** will both be switched off if set from the command
6. after execution of CALL, UPDATE, INSERT or DELETE **Cmd_OK** or **Cmd_Errror** will set depending of the result
7. if an error occured (**Cmd_Errror** is set), the MYSQL error code is shown on **SQL_Error_Stat**, an error message is available on **SQL_Error_Msg**
8. when executing SELECT successfully, at first **ColNames_Avail** is set to indicate that the number of columns is shown on **ColCount** and their names on **Columns**
9. afther that each selected row is shown on **Rows** with **New_Query_Record** set
10. it is possible, that a new row is available at each cycle so that **New_Query_Record** is permanently on
11. after the last row **Cmd_OK** is set and **Ready** will be set in the next cycle
12. if an error occurs, **Cmd_Errror** can get activated at any time and **Ready** will be set in the next cycle
13. make sure that **Ready** is set before issuing the next command, oherwise it will be ignored
14. if the connection drops for some reason, there will be an automatic attempt to re-connect every 3 sec


